package com.example.android_part_h_task_1;

/*

1. Используя API сервиса https://restcountries.eu/ создать приложение с одним экраном.

Приложение должно содержать список стран, поле для ввода данных и кнопку фильтрации.

В текстовое поле пользователь вводит число, нажимает кнопку,
в списке появляются страны, у которых население соответствует указанному числу +/-15%.

При старте приложения должен посылаться запрос на получение данных с сервера и их сохранение в локальную DB.

Все фильтрации происходят с данными в локальной базе.

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_h_task_1.model.Country;
import com.example.android_part_h_task_1.utils.Helper;
import com.example.android_part_h_task_1.utils.IHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    CountryAdapter countryAdapter;

    @BindView(R.id.am_population_number)
    EditText populationNumber;

    @BindView(R.id.am_get_country_info)
    Button getCountryInfo;

    @BindView(R.id.am_country_list)
    RecyclerView recyclerView;

/*
    private IHelper iHelper = new IHelper() {
        @Override
        public void getCountries(List<Country> countries) {
            countryAdapter = new CountryAdapter(MainActivity.this, countries);
            recyclerView.setAdapter(countryAdapter);
        }
    };
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        deleteDatabase("CountryDataBase");

        final Helper helper = new Helper();
        helper.getInfoFromServer(MainActivity.this); // Get info from server

        getCountryInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int population = Integer.parseInt(populationNumber.getText().toString().trim());

                int minNumber = population - ((population * 15) / 100);
                int maxNumber = population + ((population * 15) / 100);

                IHelper iHelper = new IHelper() {
                    @Override
                    public void getCountries(List<Country> countries) {
                        countryAdapter = new CountryAdapter(MainActivity.this, countries);
                        recyclerView.setAdapter(countryAdapter);
                    }
                };

              helper.getInfoFromDataBase(minNumber, maxNumber, iHelper); // Get info from database
            }
        });
    }
}