package com.example.android_part_h_task_1.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Country {

    // поля public можно использовать,
    // когда создаются модели для JSON
    // или для БД

    @PrimaryKey
    private int id;
    private String name;
    private int population;

    // должен быть обязательно пустой конструктор,
    // которым пользуется Room (если нет другого конструктора)
    //  public Country(){}

    public Country(int id, String name, int population){
        this.id = id;
        this.name = name;
        this.population = population;
    }

    //================================================================================//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}


